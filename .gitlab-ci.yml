# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

.templates_sha: &template_sha 18194044f0f984c8815bc9a1a146582f6bf15d41 # see https://docs.gitlab.com/ee/ci/yaml/#includefile

include:
  # Fedora container builder template
  - project: 'freedesktop/ci-templates'
    ref: *template_sha
    file: '/templates/fedora.yml'

variables:
  FDO_DISTRIBUTION_PACKAGES: 'python3-flake8 python3-parse python3-pyudev python3-pypandoc pandoc python3-pytest python3-libevdev python3-click python3-pyyaml python3-pip jq'
  FDO_DISTRIBUTION_VERSION: 33
  FDO_UPSTREAM_REPO: 'libevdev/hid-tools'
  FDO_DISTRIBUTION_TAG: '2020-01-15.1'
  QEMU_TAG:    'qemu-vm-2020-01-15.3'

stages:
  - prep
  - build
  - test
  - analysis

container-prep:
  extends:
    - .fdo.container-build@fedora
  stage: prep
  variables:
    GIT_STRATEGY: none

qemu-prep:
  extends:
    - .fdo.qemu-build@fedora
  stage: prep
  tags:
    - kvm
  variables:
    GIT_STRATEGY: none
    FDO_DISTRIBUTION_TAG: $QEMU_TAG

.default_image:
  extends:
    - .fdo.distribution-image@fedora
  needs:
    - container-prep

flake:
  extends: .default_image
  stage: build
  script:
    # The proper way is to run:
    # python3 setup.py flake8
    # but we want to also check the `tests` folder, so call flake8 directly
    - flake8

install:
  extends: .default_image
  stage: build
  script:
    - python3 setup.py install

install_no_man_pages:
  extends: .default_image
  stage: build
  script:
    - dnf remove -y pandoc python3-pypandoc
    - python3 setup.py install

.qemu_image:
  extends:
    - .fdo.distribution-image@fedora
  tags:
    - kvm
  variables:
    FDO_DISTRIBUTION_TAG: $QEMU_TAG
  needs:
    - qemu-prep

pytest:
  extends: .qemu_image
  stage: test
  script:
    - echo "[pytest]" > pytest.ini
    - echo "junit_family=xunit2" >> pytest.ini
    - /app/vmctl start || (echo "Error - Failed to start the VM." && exit 1)
    - "scp -r $PWD vm:"
    - /app/vmctl exec "$CI_PROJECT_NAME/.gitlab-ci/modprobe-hid.sh"
    - /app/vmctl exec "cd $CI_PROJECT_NAME ; pytest -v --junitxml=junit-$CI_JOB_NAME.xml ; echo \$? > .exit_code " || true
    - scp -r "vm:$CI_PROJECT_NAME/junit-*.xml" .
    - scp -r "vm:$CI_PROJECT_NAME/.exit_code" .
    - cat '.exit_code'
    - /app/vmctl stop
    # pytest uses 2 and above for any errors outside the tests themselves
    - if [[ `cat .exit_code` -gt 1 ]] ;
      then
        exit 1 ;
      fi
  artifacts:
    name: "qemu-pytest-logs-$CI_JOB_NAME"
    when: always
    expire_in: 1 week
    paths:
      - junit-*.xml
      - console.out
    reports:
      junit: junit-*.xml
  variables:
    FDO_DISTRIBUTION_TAG: $QEMU_TAG

pytest errors:
  extends:
    - .fdo.distribution-image@fedora
  stage: analysis
  script:
    - pip install jq yq
    - errors=`cat junit-*.xml | xq -r '.testsuites.testsuite."@errors"'`
    - test $errors -ne 0 && echo "pytest encountered $errors errors, please fix"
    - echo "See the previous stage for test logs"
    - exit $errors
  allow_failure: false

pytest failures:
  extends:
    - .fdo.distribution-image@fedora
  stage: analysis
  script:
    - pip install jq yq
    - failures=`cat junit-*.xml | xq -r '.testsuites.testsuite."@failures"'`
    - test $failures -ne 0 && echo "pytest encountered $failures test case failures"
    - echo "See the previous stage for test logs"
    - exit $failures
  allow_failure: true
